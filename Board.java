public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		this.tictactoeBoard = new Square[3][3];
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		int col = 0;
		int row = 0;
		System.out.print(" ");
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			System.out.print(" "+col);;
			col++;
		}
		System.out.println();
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			System.out.print(row + " ");
			row++; 
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				System.out.print(this.tictactoeBoard[i][j] + " ");
			}
			System.out.println();
		}
		return "";
	}
	
	public boolean placeToken(int row, int column, Square playerToken){
		if(row >=3 && row <= 0 || column >=3 && column <= 0){
			return false;
		}
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				if(this.tictactoeBoard[row][column] == Square.BLANK){
					this.tictactoeBoard[row][column] = playerToken;
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}
	
	public boolean checkIfFull(){
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				if(this.tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	/*better solution? I wanted to put this.tictactoeBoard - 1 
	  or something like that instead of hard coding numbers but then the code
	  doesn't work how it should. 2D arrays confuses me*/
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				if( this.tictactoeBoard[0][j] == playerToken &&
					this.tictactoeBoard[1][j] == playerToken &&
					this.tictactoeBoard[2][j] == playerToken ){
					return true;
				}
			}
		}
		return false;
	}
	//same thing here, what would be the best solution? Because I know I shouldn't be putting magic numbers 
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i = 0; i<this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				if( this.tictactoeBoard[i][0] == playerToken &&
					this.tictactoeBoard[i][1] == playerToken &&
					this.tictactoeBoard[i][2] == playerToken ){
					return true;
				}
			}
		}
		return false;
	}
	
	
	
	public boolean checkIfWinning(Square playerToken){
		boolean winningHorizontal = checkIfWinningHorizontal(playerToken);
		boolean winningVertical = checkIfWinningVertical(playerToken);
		if(winningHorizontal || winningVertical){
			return true;
		}
		return false;
	}
	
}