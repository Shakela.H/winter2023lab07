import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Welcome to the Tic-Tac-Toe game!");
		System.out.println("Player 1's token: X");
		System.out.println("Player 2's token: O");
	
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
	
		while(!gameOver){
			System.out.println();
			System.out.println(board);
			if(player == 1){
				playerToken = Square.X;
				System.out.println("Player 1: it is your turn, where do you want to place your token?");
			}else{
				playerToken = Square.O;
				System.out.println("Player 2: it is your turn, where do you want to place your token?");
			}
			
			System.out.println("Enter 2 numbers. First number represents the row, second represents the column");
			
			int row = sc.nextInt();
			int column = sc.nextInt();
			
			while(!board.placeToken(row, column, playerToken)){
				System.out.println("Please neter valid values");
				row = sc.nextInt();
				column = sc.nextInt();
			};
			if(board.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
			}else if(board.checkIfWinning(playerToken)){
				System.out.println("Player: " + player + " is the Winner!");
				gameOver = true;
			}else{
				player++;
				if (player > 2){
					player = 1;
				}
			}
			
		}
	
	}
}